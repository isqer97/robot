import RPi.GPIO as gpio
import time



def vision():
    gpio.setmode(gpio.BOARD)
    gpio.setup(16, gpio.IN)
    gpio.setup(18, gpio.IN)
    
    print("left",gpio.input(16))
    print("right",gpio.input(18))
    time.sleep(0.5)
    gpio.cleanup()
    
while True:
    vision()